#ifndef BUSYTIMEOUTACTION_H
#define BUSYTIMEOUTACTION_H

#include <string>
#include <vector>
#include <set>
#include <map>

#include "dal/Partition.h"

#include "config/Configuration.h"
#include "config/ConfigObject.h"

namespace ROS {
   class BusyTimeoutAction {
   public:
     BusyTimeoutAction () {};
     virtual ~BusyTimeoutAction () {};

     virtual void setup (Configuration *confDB, 
			 const daq::core::Partition* partition,
			 std::string& rcd_uid,
			 std::vector <std::string>& busychannel_uid_vector,
			 std::vector <bool>& enabled_config,
                         bool debug = false);

     virtual void action () = 0;
     virtual std::string get_controller (std::string &busychannel_uid, bool debug = false) = 0;
     virtual bool get_busy_source (std::string &busychannel_uid,
				   std::vector <std::pair <std::string, std::string> > &source) = 0;

   protected:
     std::string m_rcd_name;
     std::map <std::string, std::string> m_busy_source;
     std::map <std::string, std::string> m_rcd_map;
     std::map <std::string, std::string> m_controller_map;

     Configuration* m_oks_db;
     const daq::core::Partition* m_partition;

   private:
     std::string m_current_rcd;
     std::string m_current_controller;
     std::map <const daq::core::Segment *, bool> m_is_linked;

   private:
     bool find_segment (const daq::core::Partition *partition,
			const daq::core::Segment *target);

     bool navigate_segments (const daq::core::Segment *segment,
			     const daq::core::Segment *target);

     void navigate_relationships (Configuration *confDB,
				  ConfigObject *o,
				  const daq::core::Partition* partition,
				  bool debug = false);
   };
}
#endif //BUSYTIMEOUTACTION_H
