#ifndef DEFAULTBUSYTIMEOUTACTION_H
#define DEFAULTBUSYTIMEOUTACTION_H

#include "RODBusyModule/BusyTimeoutAction.h"

namespace ROS {
  class DefaultBusyTimeoutAction : public BusyTimeoutAction {
   public:
    DefaultBusyTimeoutAction () {};
    virtual ~DefaultBusyTimeoutAction () {};

    virtual void action () {};
    virtual std::string get_controller (std::string &busychannel_uid, bool debug = false);
    virtual bool get_busy_source (std::string &busychannel_uid,
				  std::vector <std::pair <std::string, std::string> > &source);
  };
}

#endif //DEFAULTBUSYTIMEOUTACTION_H
