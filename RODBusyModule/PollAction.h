#ifndef POLLACTION_H
#define POLLACTION_H

#include <list>
#include <vector>
#include <string>

#include "ROSCore/ScheduledUserAction.h"
#include "RODBusy/RODBusy.h"
#include "RODBusyModule/BusyTimeoutAction.h"

#include <mutex>

namespace ROS {

  class PollAction : public ScheduledUserAction {
  public:
    
    PollAction(RCD::RODBusy* rodbusy, std::string UID, bool emulateVME=false);

    virtual ~PollAction();
    virtual void reactTo();
    void enablePolling();
    void disablePolling();
    void reset();

    void getBusyRate(std::vector<double>& rate, u_int& nwords);

    //
    // The action also watches for timeouts
    // 
    bool watch_enable();
    bool watch_disable();
    bool watch_enable(unsigned int channel);
    bool watch_disable(unsigned int channel);
    void watch_reset();
    bool watch_reset(unsigned int channel);
    bool watch_setThreshold(double watch_threshold); // busy fraction threshold (fraction, not percentage!)
    bool watch_getThreshold(double& watch_threshold, unsigned int& watch_threshold_uint);
    bool watch_setTimeout(unsigned int channel, unsigned int timeout);
    bool watch_setRecoveryTimeout(unsigned int channel, unsigned int timeout);
    bool watch_set(double watch_threshold,
		   std::vector<unsigned int>& timeout,
		   std::vector<unsigned int>& recovery_timeout, std::vector<bool>& enable);
    void watch_get(double& watch_threshold,
		   unsigned int& watch_threshold_uint,
		   std::vector<unsigned int>& timeout,
		   std::vector<unsigned int>& recovery_timeout, std::vector<unsigned int>& counter);
    void setup_issue_reporting(std::vector<std::string>& channel_uid, std::string& rcd_uid);

    // Set up user plugin
    bool timeout_plugin_set (BusyTimeoutAction *timeout_plugin);
    bool recovery_plugin_set (BusyTimeoutAction *recovery_plugin);

  private:
    RCD::RODBusy* m_rodbusy;
    bool m_emulateVME;
    std::vector<double> m_busyrate;
    u_int m_nwords; // number of fifo words used to calculate the busyrate

    std::string m_UID;
    std::string m_rcd_uid;
    bool m_enabled;
    u_short m_interval;
    unsigned int m_status;

    bool m_watch_global_enabled;
    std::vector<bool> m_watch_enabled;
    double m_watch_threshold;
    unsigned int m_watch_threshold_uint;
    std::vector<unsigned int> m_watch_timeout_5ms;
    std::vector<unsigned int> m_watch_recovery_timeout_5ms;
    std::vector<unsigned int> m_watch_busycounter_5ms;
    std::vector<std::string> m_busychannel_uid;

    std::mutex m_mutex; // to protect shared memory

    // Timeout and recovery user plugin
    BusyTimeoutAction *m_timeout_plugin;
    BusyTimeoutAction *m_recovery_plugin;
  }; //class PollAction.

}//namespace ROS
#endif // POLLACTION_H
