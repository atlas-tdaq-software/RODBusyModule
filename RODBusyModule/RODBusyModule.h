#ifndef RODBUSYMODULE_H
#define RODBUSYMODULE_H

#include "RODBusyModule/PollAction.h"
#include "RODBusyModule/BusyTimeoutAction.h"

#include "ROSUtilities/PluginFactory.h"
#include "DFSubSystemItem/Config.h"
#include "rcc_rodbusy/rcc_rodbusy.h"
#include "ROSCore/ReadoutModule.h"
#include "rc/RCStateInfo.h"
#include "ipc/partition.h"
#include "is/info.h"
#include "is/infoany.h"
#include "is/infodictionary.h"
#include "is/inforeceiver.h"
#include "is/infodocument.h"
#include "is/type.h"

#include <string>
#include <vector>

namespace ROS {

  class RODBusyModule : public ReadoutModule {

  public:

    RODBusyModule();
    virtual ~RODBusyModule() noexcept;

    virtual const std::vector<DataChannel*>* channels();

    virtual void setup(DFCountedPointer<Config> configuration);

    virtual void clearInfo() ;
    virtual DFCountedPointer<Config> getInfo() ;

    virtual void configure(const daq::rc::TransitionCmd&) ;
    virtual void unconfigure(const daq::rc::TransitionCmd&) ;
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopROIB(const daq::rc::TransitionCmd&);

    virtual void disable(const std::vector<std::string>& components);
    virtual void enable(const std::vector<std::string>& components);

    virtual void userCommand(std::string &commandName,
			     std::string &argument);

    virtual void publish();
    virtual void publishFullStats();

    virtual void startWatchDog();
    virtual void stopWatchDog();

    // Return action plugin for recovery and timeout actions
    BusyTimeoutAction *timeout_plugin();
    BusyTimeoutAction *recovery_plugin();

    virtual void rc_callback(ISCallbackInfo* isc);

  private:
    // helper function for disable/enable
    std::vector<std::string> parseCommaDeliminatedStrings(std::string s);

    void subscribe();
    void unsubscribe();
    void handleRCStateInfo(RCStateInfo&);

    void updateBusyMask();

    // Some internal variables
    unsigned int m_status;

    RCD::RODBusy* m_rodbusy;
    IPCPartition m_partition;
    PollAction* m_PollAction;

    // database variables
    std::string m_UID;
    std::string m_rcd_uid;
    bool m_RC_in_running_state;
    u_int m_PhysAddress;
    bool m_useConfiguration;
    std::string m_MonitoringLevel;
    std::string m_IS_ServerName;
    u_int m_IS_UpdatePeriod;
    u_int m_ServiceRequest_Limit;
    u_int m_ServiceRequest_ResetPeriod;
    bool m_EmulateVME;
    bool m_mon_detailed;
    bool m_mon_basic;
    unsigned int m_mask_config;
    unsigned int m_mask_current;
    u_short m_transferInterval;
    double m_timeOutBusyFractionThreshold; // fraction, not percent!
    std::vector<unsigned int> m_timeout_sec;
    std::vector<unsigned int> m_recovery_timeout_sec;
    std::vector<bool> m_enabled_config;
    std::vector<bool> m_enabled_current;
    std::vector<std::string> m_busychannel_uid;
    std::ostringstream m_msg;

    // plug-in and factories for user actions in case of recovery or timeout
    PluginFactory <BusyTimeoutAction>  m_timeoutFactory;
    BusyTimeoutAction                  *m_timeout_plugin;
    BusyTimeoutAction                  *m_recovery_plugin;

    std::string m_sub;
    ISInfoReceiver* m_isrec;
    ISInfoDictionary* m_isdict;

  }; //class RODBusyModule

  inline const std::vector<DataChannel*>* RODBusyModule::channels() {
    return 0;
  }

  inline BusyTimeoutAction *RODBusyModule::timeout_plugin() {
    return m_timeout_plugin;
  }

  inline BusyTimeoutAction *RODBusyModule::recovery_plugin() {
    return m_recovery_plugin;
  }
} //namespace ROS

#endif //RODBUSYMODULE_H
