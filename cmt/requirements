package RODBusyModule

author  Thilo.Pauly@cern.ch
manager Thilo.Pauly@cern.ch

public

use TDAQCPolicy
use genconfig
use rcc_error
use vme_rcc
use DFThreads
use ROSCore
use ROSInterruptScheduler
use DFSubSystemItem
use ROSMemoryPool
use ROSEventFragment
use ROSBufferManagement
use RCDUtilities
use DFConfiguration
use is
use RODBusy
use RCDVme
use RunControl
use ers
use dal
use TTCInfo

private
branches RODBusydal
macro generate-config-classes "RODBusyModule BusyTimeoutAction BusyChannel"
macro generate-config-include-dirs "${TDAQ_INST_PATH}/share/data/dal ${TDAQ_INST_PATH}/share/data/DFConfiguration"

document generate-config  RODBusydal -s=../schema/ namespace="RODBusydal" include="RODBusydal" packagename="RODBusydal" RODBusy.schema.xml

macro   gdir  "$(bin)RODBusydal.tmp"
library rodbusydal $(gdir)/*.cpp

macro rodbusydal_dependencies "RODBusydal"
#macro RODBusydal_libraries    "rodbusydal"
macro rodbusydal_shlibflags "-ldaq-df-dal"

application RODBusydal_dump -no_prototypes $(gdir)/dump/dump_RODBusydal.cpp
macro       RODBusydal_dumplinkopts "-lrodbusydal -lipc"
macro       RODBusydal_dump_dependencies "rodbusydal generate-dal"

ignore_pattern install_headers_bin_auto
apply_pattern install_headers src_dir="$(bin)/RODBusydal" files="*.hh *.h *.java" target_dir="../RODBusydal/"
apply_pattern install_libs files="librodbusydal.so"
apply_pattern install_apps files="RODBusydal_dump"

macro RODBusyModule_dependencies "rodbusydal"

library RODBusyModule -suffix=local "RODBusyModule.cpp \
                                     PollAction.cpp \
                                     BusyTimeoutAction.cpp "

macro RODBusyModule_shlibflags "-lRODBusy -lrodbusydal \
                                -lSubSystemItem \
				-lgetinput -lrcc_time_stamp \
				-lDFDebug -lRCDVme \
				-lvme_rcc -lcmem_rcc -lio_rcc -lrcc_error \
				-lROSCore -lROSUserActionScheduler \
				-lRCDUtilities \
				-lis -lipc -lomniORB4 -lowl -lomnithread -lers"

macro DefaultBusyTimeoutAction_dependencies "RODBusyModule"

library DefaultBusyTimeoutAction -suffix=local "DefaultBusyTimeoutAction.cpp"

macro DefaultBusyTimeoutAction_shlibflags "-lRODBusyModule"

# test application

application test_rodbusy_action -no_prototypes test/test_rodbusy_action.cpp

macro       test_rodbusy_actionlinkopts "-lDefaultBusyTimeoutAction \
                                         -lRODBusyModule \
                                         -lrodbusydal \
                                         -ldaq-df-dal \
                                         -lipc"

macro       test_rodbusy_action_dependencies   "RODBusyModule \
                                                DefaultBusyTimeoutAction \
                                                rodbusydal generate-dal"

public
apply_pattern install_libs files="libRODBusyModule.so \
                                  libDefaultBusyTimeoutAction.so"

apply_pattern install_apps files="test_rodbusy_action"

apply_pattern build_jar name="RODBusydal" src_dir="$(gdir)/RODBusydal" sources="*.java"
macro RODBusydal.jar_dependencies "RODBusydal"

apply_pattern install_jar name="RODBusydal" files=RODBusydal.jar
macro sw.repository.java.RODBusydal.jar:description            "jar for the Central Expert System (CES)"

apply_pattern install_db_files name=hw         src_dir="../hw"         files="*.xml"                 target_dir="hw"
apply_pattern install_db_files name=sw         src_dir="../sw"         files="*.xml"                 target_dir="sw"
apply_pattern install_db_files name=segment    src_dir="../segments"   files="*.xml"                 target_dir="segments"
apply_pattern install_db_files name=partition  src_dir="../partitions" files="*.xml"                 target_dir="partitions"
apply_pattern install_db_files name=oks_schema src_dir="../schema"     files="RODBusy.schema.xml"    target_dir="schema"
