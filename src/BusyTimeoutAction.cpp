#include "config/Schema.h"
#include "config/Configuration.h"
#include "config/ConfigObject.h"

#include "dal/Partition.h"
#include "dal/Segment.h"
#include "dal/ResourceBase.h"
#include "dal/RunControlApplicationBase.h"

#include "DFdal/ReadoutApplication.h"

#include "RODBusydal/BusyChannel.h"
#include "RODBusyModule/BusyTimeoutAction.h"

bool ROS::BusyTimeoutAction::find_segment (const daq::core::Partition* partition,
					   const daq::core::Segment *target)
{
  std::vector <const daq::core::Segment *> vseg;
  std::vector <const daq::core::Segment *>::iterator iseg;

  vseg = partition->get_Segments ();

  for (iseg = vseg.begin (); iseg != vseg.end (); ++iseg)
  {
    if ((*iseg)->UID () == target->UID ())
      return true;
    else if (this->navigate_segments ((*iseg), target))
      return true;
  }

  return false;
}

bool ROS::BusyTimeoutAction::navigate_segments (const daq::core::Segment *segment,
						const daq::core::Segment *target)
{
  std::vector <const daq::core::Segment *> vseg;
  std::vector <const daq::core::Segment *>::iterator iseg;

  vseg = segment->get_Segments ();

  for (iseg = vseg.begin (); iseg != vseg.end (); ++iseg)
  {
    if ((*iseg)->UID () == target->UID ())
      return true;
    else if (this->navigate_segments ((*iseg), target))
      return true;
  }

  return false;
}

void ROS::BusyTimeoutAction::navigate_relationships (Configuration *confDB,
						     ConfigObject *o,
						     const daq::core::Partition *partition,
						     bool debug)
{
  std::string class_name = o->class_name ();

  // If the object is a resource, then check if it is enabled

  const daq::core::ResourceBase *res_obj = confDB->get <daq::core::ResourceBase> (class_name);

  if (res_obj)
    if (res_obj->disabled (*partition))
      return;

  std::vector <ConfigObject> values;
  const std::string relationship_name = "*";
  bool check_composite_only = false;
  unsigned long rlevel = 0;
  const std::vector <std::string> *rclasses = 0;

  try
  {
    o->referenced_by (values, relationship_name, check_composite_only,
		      rlevel, rclasses);
  }

  catch (...)
  {
    return;
  }

  std::vector <ConfigObject>::iterator iobj;

  if (debug)
  {
    std::cout << "Object " << o->full_name () << " is referenced by : ";

    for (iobj = values.begin (); iobj !=values.end () ; ++iobj)
    {
      ConfigObject *conf_obj = &(*iobj);
      std::cout << conf_obj->full_name () << ",";
    }

    std::cout << std::endl;
  }

  for (iobj = values.begin (); iobj !=values.end () ; ++iobj)
  {
    if (iobj->class_name () == "Segment")
    {
      const daq::core::Segment *s = confDB->get <daq::core::Segment> (iobj->UID ());

      if (debug)
	std::cout << "Segment is " << s->UID () << std::endl;

      // Is that segment disabled ?

      if (s->disabled (*partition))
	continue;

      if (debug)
	std::cout << "Segment " << s->UID () << " is enabled " << std::endl;

      // Is that segment part of the partition ?

      std::pair <const daq::core::Segment *, bool> is_linked;
      std::map <const daq::core::Segment *, bool>::iterator link_it;

      link_it = m_is_linked.find (s);

      if (link_it != m_is_linked.end ())
      {
	is_linked = *link_it;
      }
      else
      {
	is_linked.first = s;
	is_linked.second = this->find_segment (partition, s);

	m_is_linked.insert (is_linked);
      }

      if (! is_linked.second)
	continue;

      if (debug)
	std::cout << "Segment " << s->UID () << " is linked " << std::endl;

      // Find the controller (works also for template segments)

      const daq::core::Segment *seg_conf;
      seg_conf = partition->get_segment (s->UID());

      const daq::core::BaseApplication* app_config = seg_conf->get_controller ();
      m_current_controller = app_config->UID ();

      return;
    }
    else if (iobj->class_name () != "BusyChannel" &&
	     iobj->class_name () != "RODBusyModule" &&
             iobj->full_name () != o->full_name ())
    {
      ConfigObject *conf_obj = &(*iobj);

      this->navigate_relationships (confDB, conf_obj, partition, debug);

      if (iobj->class_name () == "RCD")
      {
	const daq::df::ReadoutApplication *rapp = confDB->get <daq::df::ReadoutApplication> (iobj->UID ());
	if (! rapp->disabled (*partition))
	{
	  m_current_rcd = rapp->UID ();
	  return;
	}
      }
    }
  }
}

void ROS::BusyTimeoutAction::setup (Configuration *confDB,
				    const daq::core::Partition *partition,
				    std::string& rcd_uid,
				    std::vector <std::string>& busychannel_uid_vector,
				    std::vector <bool>& enabled_config,
				    bool debug)
{
  m_busy_source.clear ();
  m_rcd_map.clear ();
  m_controller_map.clear ();
  m_is_linked.clear ();

  m_oks_db = confDB;
  m_rcd_name = rcd_uid;
  m_partition = partition;

  // Get the vector of busy sources

  int i;
  std::vector <std::string>::iterator ichannel;

  for (i = 0, ichannel = busychannel_uid_vector.begin ();
       ichannel != busychannel_uid_vector.end (); ++ichannel, ++i)
  {
    if (enabled_config[i])
    {
      const RODBusydal::BusyChannel *bc = confDB->get <RODBusydal::BusyChannel> ((*ichannel));
      const daq::core::ResourceBase *rb = bc->get_BusySource ();
      std::string obj_name = rb->UID () + "@" + rb->class_name ();
      std::pair<std::string, std::string> busy_channel_pair;

      busy_channel_pair.first = busychannel_uid_vector[i];
      busy_channel_pair.second = obj_name;

      m_busy_source.insert (busy_channel_pair);

      // Get the busy source config object

      ConfigObject *conf_obj = const_cast <ConfigObject *> (&(rb->config_object ()));

      // Navigate back to the segment

      m_current_controller = "";
      this->navigate_relationships (confDB, conf_obj, partition, debug);

      // Fill maps

      std::pair <std::string, std::string> rcd_map_element;
      std::pair <std::string, std::string> controller_map_element;

      rcd_map_element.first = busychannel_uid_vector[i];
      rcd_map_element.second = m_current_rcd;

      controller_map_element.first = busychannel_uid_vector[i];
      controller_map_element.second = m_current_controller;

      m_rcd_map.insert (rcd_map_element);
      m_controller_map.insert (controller_map_element);
    }
  }

  m_is_linked.clear ();

  if (debug)
  {
    std::map <std::string, std::string>::iterator j, k;

    for (j = m_rcd_map.begin (), k = m_controller_map.begin ();
	 k != m_controller_map.end (); ++j, ++k)
      std::cout << (*k).first << " " 
		<< (*k).second << " "
		<< (*j).second << std::endl;
  }
}
