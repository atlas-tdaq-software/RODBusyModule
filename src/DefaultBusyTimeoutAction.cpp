#include "RODBusyModule/DefaultBusyTimeoutAction.h"

std::string ROS::DefaultBusyTimeoutAction::get_controller (std::string &busychannel_uid, bool debug)
{
  if (debug)
    for (std::map<std::string, std::string>::iterator k = m_controller_map.begin ();
	 k != m_controller_map.end (); ++k)
      std::cerr << (*k).first << " " << (*k).second << std::endl;

  return m_controller_map[busychannel_uid];
}

bool ROS::DefaultBusyTimeoutAction::get_busy_source (std::string &busychannel_uid,
						     std::vector <std::pair <std::string, std::string> > &source)
{
  std::pair <std::string, std::string> s;

  source.clear (); // In case an unclean vector is passed by the calling class

  s.first = busychannel_uid;
  s.second = m_rcd_name;

  source.push_back (s);

  return true;
}

// This is the hook for the plugin factory

extern "C"
{
  extern ROS::BusyTimeoutAction *createDefaultBusyTimeoutAction ();
}

ROS::BusyTimeoutAction *createDefaultBusyTimeoutAction ()
{
  return (new ROS::DefaultBusyTimeoutAction ());
}
