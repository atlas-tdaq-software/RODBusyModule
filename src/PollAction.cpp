#include "RODBusyModule/PollAction.h"
#include "RODBusyModule/BusyTimeoutAction.h"

#include "RCDVme/RCDVme.h"
#include "RODBusy/RODBusy.h"
#include "ers/ers.h"

// For error catching
#include "ROSUtilities/ROSErrorReporting.h"
#include "RunControl/Common/UserExceptions.h"
#include <sstream>

using namespace RCD;
using namespace ROS;

PollAction::PollAction(RCD::RODBusy* rodbusy, std::string UID, bool emulateVME)
  : 
  ScheduledUserAction(250), // use 250ms as polling period (must be smaller than 2.5 seconds
  m_rodbusy(rodbusy),
  m_emulateVME(emulateVME),
  m_busyrate(16),
  m_nwords(0),
  m_UID(UID),
  m_rcd_uid(""),
  m_enabled(false),
  m_interval(0),
  m_status(0),
  m_watch_global_enabled(false),
  m_watch_enabled(16),
  m_watch_threshold(100.),
  m_watch_threshold_uint(0xc350), // 0xc350=50000 corresponds to 5ms, i.e. 100%
  m_watch_timeout_5ms(16),
  m_watch_recovery_timeout_5ms(16),
  m_watch_busycounter_5ms(16),
  m_busychannel_uid(16),
  m_timeout_plugin(0),
  m_recovery_plugin(0)
{
  m_rcd_uid = "";
  std::string sout = m_UID + "@PollAction::PollAction() ";
  ERS_LOG(sout << "Entered");

  m_mutex.lock();
  m_nwords = 0;
  for (unsigned short i(0); i < 16; ++i) {
    m_busyrate[i] = 0.0;
    m_watch_enabled[i] = false;
    m_watch_timeout_5ms[i] = 0;
    m_watch_recovery_timeout_5ms[i] = 0;
    m_watch_busycounter_5ms[i] = 0;
  }
  m_mutex.unlock();


  for (unsigned short i(0); i < 16; ++i) {
    m_busychannel_uid[i] = "";
  }

  // resets
  this->reset();

  m_status = m_rodbusy->getSequencerTransferInterval(m_interval);
  ERS_LOG(sout << "Done");
}

PollAction::~PollAction(void) {
  std::string sout = m_UID + "@PollAction::~PollAction() ";
  ERS_LOG(sout << "Entered");

  ERS_LOG(sout << "Done");
}

void PollAction::reset(void) {
  std::string sout = m_UID + "@PollAction::reset() ";
  ERS_LOG(sout << "Entered");

  m_mutex.lock();
  m_nwords = 0;
  for (unsigned short i(0); i < 16; ++i) {
    m_busyrate[i] = 0.0;
    m_watch_busycounter_5ms[i] = 0;
  }
  m_mutex.unlock();

  ERS_LOG(sout << "Done");
}


void PollAction::reactTo(void) {
  std::string sout = m_UID + "@PollAction::reactTo() ";

  if (m_enabled) {

    m_status = 0;

    // get interval (real interval is +1)
    u_short interval(0);
    m_status |= m_rodbusy->getSequencerTransferInterval(interval);
    if (interval != m_interval) {
      ERS_LOG(sout << "Reset interval seems to have changed from " << m_interval << " to " << interval);
      //       ERS_ERROR(sout << "Reset interval seems to have changed from " << m_interval << " to " << interval);
      m_interval = interval;
    }
    ++interval;

    u_short eflags(0);
    m_status |= m_rodbusy->getFifoEmptyFlags(eflags);

    u_int nwords(0);
    std::vector<u_int> busy(16);
    for (int i = 0; i < 16; ++i) {
      busy[i] = 0;
    }

    u_short w(0);
    while ( (m_status == 0) && (eflags == 0x0) && (nwords < 512) ){

      for (int i = 0; i < 16; ++i) {
	m_status |= m_rodbusy->readWordFromFifo(i, w);
	busy[i] += w;

	// watch for timeouts
	m_mutex.lock();
	
	if (m_watch_global_enabled && m_watch_enabled[i]) {
	  // if channel has full busy fraction
	  if (w >= m_watch_threshold_uint) { 
	    ++m_watch_busycounter_5ms[i];

	    // Check if recovery action has to be triggered

            if (m_watch_busycounter_5ms[i]==m_watch_recovery_timeout_5ms[i]) {
	      ERS_LOG(sout << "Recovery timeout on " << m_busychannel_uid[i] << "@BusyChannel (Id=" << i << ") detected");

	      if (m_recovery_plugin) {
		m_recovery_plugin->action ();

		std::string source_controller = m_recovery_plugin->get_controller (m_busychannel_uid[i]);
		daq::rc::HardwareSynchronization issue(ERS_HERE, 
						       source_controller.c_str ());
		ers::error(issue);
	      }
	    }

            // Check if timeout has been hit

	    if (m_watch_busycounter_5ms[i]==m_watch_timeout_5ms[i]) {
	      ERS_LOG(sout << "Timeout on " << m_busychannel_uid[i] << "@BusyChannel (Id=" << i << ") detected");

	      bool disable_watchdog = true;
	      std::string busychannel_name = m_busychannel_uid[i];

              if (m_timeout_plugin)
	      {
		std::vector <std::pair <std::string, std::string> > busy_sources;
		std::vector <std::pair <std::string, std::string> >::iterator isource;

		m_timeout_plugin->action ();
		disable_watchdog = m_timeout_plugin->get_busy_source (busychannel_name, busy_sources);

		for (isource = busy_sources.begin (); isource != busy_sources.end (); ++isource)
		{
		  std::pair <std::string, std::string> s = *isource;

		  std::string hg_busychannel_name = s.first;
		  std::string rcd_name = s.second;

		  daq::rc::HardwareError issue (ERS_HERE, hg_busychannel_name.c_str (), rcd_name.c_str ());
		  ers::error (issue);
		}
	      }
	      else
	      {
		daq::rc::HardwareError issue (ERS_HERE, busychannel_name.c_str (), m_rcd_uid.c_str ());
		ers::error (issue);
	      }

	      if (disable_watchdog)
		m_watch_enabled[i] = false;
	    }
	  } else {
	    m_watch_busycounter_5ms[i] = 0;
	  }
	}
	m_mutex.unlock();
      }
      ++nwords;

      m_status |= m_rodbusy->getFifoEmptyFlags(eflags);
    }
    // finished with this acquisition

    // now calculate the busy rate and add it to previous acquisitions
    m_mutex.lock();
    if ((m_nwords>0 || nwords>0) && (interval > 0)) {
      for (int i = 0; i<16; ++i) {
	m_busyrate[i] = (static_cast<double>(m_nwords)*m_busyrate[i] + 
			 static_cast<double>(busy[i])/static_cast<double>(interval))/static_cast<double>(m_nwords+nwords);
      }
      m_nwords += nwords;
    }
    m_mutex.unlock();
  }
  else {
    //Don't need to do anything if we are not reading out the data.
  }

  ERS_DEBUG(2, sout << "Done");
}

void PollAction::enablePolling() {
  std::string sout = m_UID + "@PollAction::enablePolling() ";
  ERS_LOG(sout << "Called");
  m_enabled = true;
}


void PollAction::disablePolling() {
  std::string sout = m_UID + "@PollAction::disablePolling() ";
  ERS_LOG(sout << "Called");
  m_enabled = false;
}

bool PollAction::watch_enable() {
  m_mutex.lock();
  m_watch_global_enabled = true;
  m_mutex.unlock();
  return true;
}

bool PollAction::watch_disable() {
  m_mutex.lock();
  m_watch_global_enabled = false;
  m_mutex.unlock();
  return true;
}


bool PollAction::watch_enable(unsigned int channel) {
  bool ret=false;
  m_mutex.lock();
  if (channel<16) {
    m_watch_enabled[channel] = true;
    ret = true;
  }
  m_mutex.unlock();
  return ret;
}


bool PollAction::watch_disable(unsigned int channel) {
  bool ret=false;
  m_mutex.lock();
  if (channel<16) {
    m_watch_enabled[channel] = false;
    ret = true;
  }
  m_mutex.unlock();
  return ret;
}

bool PollAction::watch_reset(unsigned int channel) {
  bool ret=false;
  m_mutex.lock();
  if (channel<16) {
    m_watch_busycounter_5ms[channel] = 0;
    ret = true;
  }
  m_mutex.unlock();
  return ret;
}

void PollAction::watch_reset() {
  m_mutex.lock();
  for (int i = 0; i<16; ++i) {
    m_watch_busycounter_5ms[i] = 0;
  }
  m_mutex.unlock();
}


bool PollAction::watch_setTimeout(unsigned int channel, unsigned int timeout_sec) {
  bool ret=false;
  m_mutex.lock();
  if (channel<16) {
    m_watch_timeout_5ms[channel] = 200*timeout_sec;
    ret = true;
  }
  m_mutex.unlock();
  return ret;
}

bool PollAction::watch_setRecoveryTimeout(unsigned int channel, unsigned int timeout_sec) {
  bool ret=false;
  m_mutex.lock();
  if (channel<16) {
    m_watch_recovery_timeout_5ms[channel] = 200*timeout_sec;
    ret = true;
  }
  m_mutex.unlock();
  return ret;
}

bool PollAction::watch_set(double watch_threshold,
			   std::vector<unsigned int>& timeout, 
                           std::vector<unsigned int>& recovery_timeout,
			   std::vector<bool>& enable) {
  bool ret=true;
  // consistency check
  if ( (timeout.size() != 16) || (enable.size() != 16) ) {
    return false;
  }
  m_mutex.lock();

  m_watch_threshold=watch_threshold;
  m_watch_threshold_uint = static_cast<unsigned int>(static_cast<double>(0xc350)*watch_threshold);

  for (unsigned int i(0); i<16; ++i) {
    if (enable[i] && timeout[i]>0) {
      m_watch_enabled[i] = true;
      m_watch_timeout_5ms[i] = timeout[i]*200;
      m_watch_recovery_timeout_5ms[i] = recovery_timeout[i]*200;
      m_watch_busycounter_5ms[i]=0;
    }
    else {
      m_watch_enabled[i] = false;
    }
  }
  m_mutex.unlock();
  
  return ret;
}

bool PollAction::watch_setThreshold(double watch_threshold) {
  bool ret=true;
  m_mutex.lock();
  m_watch_threshold=watch_threshold;
  m_watch_threshold_uint = static_cast<unsigned int>(static_cast<double>(0xc350)*watch_threshold);
  m_mutex.unlock();
  return ret;
}

bool PollAction::watch_getThreshold(double& watch_threshold, unsigned int& watch_threshold_uint) {
  bool ret=true;
  m_mutex.lock();
  watch_threshold= static_cast<double>(m_watch_threshold_uint)/static_cast<double>(0xc350);
  watch_threshold_uint = m_watch_threshold_uint;
  m_mutex.unlock();
  return ret;
}

void PollAction::watch_get(double& watch_threshold, unsigned int& watch_threshold_uint,
			   std::vector<unsigned int>& timeout,
			   std::vector<unsigned int>& recovery_timeout,
			   std::vector<unsigned int>& counter) {
  m_mutex.lock();
  watch_threshold= static_cast<double>(m_watch_threshold_uint)/static_cast<double>(0xc350);
  watch_threshold_uint = m_watch_threshold_uint;
  
  for (std::vector<unsigned int>::const_iterator it = m_watch_timeout_5ms.begin();
       it != m_watch_timeout_5ms.end();
       ++it) {
    timeout.push_back(((*it)-(*it)%200)/200);
  }
  for (std::vector<unsigned int>::const_iterator it = m_watch_recovery_timeout_5ms.begin();
       it != m_watch_recovery_timeout_5ms.end();
       ++it) {
    recovery_timeout.push_back(((*it)-(*it)%200)/200);
  }
  for (std::vector<unsigned int>::const_iterator it = m_watch_busycounter_5ms.begin();
       it != m_watch_busycounter_5ms.end();
       ++it) {
    counter.push_back(((*it)-(*it)%200)/200);
  }
  m_mutex.unlock();
}

void PollAction::setup_issue_reporting(std::vector<std::string>& channel_uid, std::string& rcd_uid) {
  m_busychannel_uid.clear();
  m_busychannel_uid = channel_uid;
  m_rcd_uid = rcd_uid;
}

void PollAction::getBusyRate(std::vector<double>& rate, u_int& nwords) {

  m_mutex.lock();
  rate = m_busyrate;
  nwords = m_nwords;

  m_nwords = 0;
  for (int i = 0; i<16; ++i) {
    m_busyrate[i] = 0.0;
  }
  m_mutex.unlock();

}

bool PollAction::timeout_plugin_set (BusyTimeoutAction *timeout_plugin)
{
  bool done = false;

  m_mutex.lock();

  if (m_watch_global_enabled == false)
  {
    m_timeout_plugin = timeout_plugin;
    done = true;
  }

  m_mutex.unlock();

  return done;
}

bool PollAction::recovery_plugin_set (BusyTimeoutAction *recovery_plugin)
{
  bool done = false;

  m_mutex.lock();

  if (m_watch_global_enabled == false)
  {
    m_recovery_plugin = recovery_plugin;
    done = true;
  }

  m_mutex.unlock();

  return done;
}
