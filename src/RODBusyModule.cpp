#include "ROSUtilities/PluginFactory.h"

#include "RODBusyModule/RODBusyModule.h"
#include "RODBusyModule/BusyTimeoutAction.h"
#include "RODBusyModule/DefaultBusyTimeoutAction.h"

#include "TTCInfo/RODBusyIS.h"

#include "RODBusydal/BusyChannel.h"
#include "RODBusydal/BusyTimeoutAction.h"
#include "RODBusydal/RODBusyModule.h"

#include "config/Configuration.h"
#include "dal/Resource.h"
#include "dal/Component.h"
#include "dal/Partition.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include "ers/ers.h"

#include "is/infodictionary.h"
#include "is/info.h"

//Standard libraries.
#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include "is/infodictionary.h"

// using namespace RCD;
using namespace ROS;

/******************************************************/
RODBusyModule::RODBusyModule()
/******************************************************/
  : m_status(0),
    m_rodbusy(0),
    m_partition(getenv("TDAQ_PARTITION")),
    m_PollAction(nullptr),
    m_UID(""),
    m_rcd_uid(""),
    m_RC_in_running_state(false),
    m_PhysAddress(0),
    m_useConfiguration(false),
    m_MonitoringLevel(""),
    m_IS_ServerName("Monitoring"),
    m_IS_UpdatePeriod(0),
    m_ServiceRequest_Limit(0),
    m_ServiceRequest_ResetPeriod(0),
    m_EmulateVME(false),
    m_mon_detailed(false),
    m_mon_basic(false),
    m_mask_config(0),
    m_mask_current(0),
    m_transferInterval(0xC34F),
    m_timeOutBusyFractionThreshold(1.),
    m_timeout_sec(16),
    m_recovery_timeout_sec(16),
    m_enabled_config(16),
    m_enabled_current(16),
    m_busychannel_uid(16),
    m_sub("RunCtrl.RootController"),
    m_isrec(0),
    m_isdict(0)
{
  m_UID = "";
  std::string sout = m_UID + "@RODBusyModule::RODBusyModule() ";
  ERS_LOG(sout << "Entered");

  int i;

  for (i=0; i<16; ++i) {
    m_timeout_sec[i]=0;
    m_recovery_timeout_sec[i]=0;
    m_enabled_config[i]=false;
    m_enabled_current[i]=false;
    m_busychannel_uid[i] = "invalid";
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
RODBusyModule::~RODBusyModule() noexcept
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::~RODBusyModule() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "Done");
}

/******************************************************************/
void RODBusyModule::setup(DFCountedPointer<Config> configuration)
/******************************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::setup() ";
  ERS_LOG(sout << "Entered");

  // initialize
  for (int i(0); i<16; ++i) {
    m_timeout_sec[i]=0;
    m_recovery_timeout_sec[i]=0;
    m_enabled_config[i]=false;
    m_enabled_current[i]=false;
    m_busychannel_uid[i] = "invalid";
  }
  
  m_status = 0;

  ERS_LOG(sout << "m_partition = " << m_partition.name());

  // get attributes from oks
  m_UID = configuration->getString("UID");
  sout = m_UID + "@RODBusyModule::setup() ";
  ERS_LOG(sout << "m_UID = " << m_UID);

  // get name of the RCD application
  // Giovanna: "for any process which is launched by the run control there is an environment variable set called $TDAQ_APPLICATION_OBJECT_ID. This means that this variable can be retrieved and gives you the UID of your application in the DB, which is identical to its name."
  //   m_rcd_uid = getenv("TDAQ_APPLICATION_OBJECT_ID");
  m_rcd_uid = configuration->getString("appName");
  ERS_LOG(sout << "m_rcd_uid = " << m_rcd_uid);

  m_PhysAddress = configuration->getUInt("PhysAddress");
  ERS_LOG(sout << "m_PhysAddress (VME base) = 0x" << std::hex << m_PhysAddress << std::dec);

  m_useConfiguration = configuration->getBool("useConfiguration");
  ERS_LOG(sout << "m_useConfiguration = " << m_useConfiguration);

  m_MonitoringLevel = configuration->getString("MonitoringLevel");
  ERS_LOG(sout << "m_MonitoringLevel = " << m_MonitoringLevel);

  if (m_MonitoringLevel == "Basic") {
    m_mon_basic = true;
    m_mon_detailed = false;
  } else if (m_MonitoringLevel == "Detailed") {
    m_mon_basic = true;
    m_mon_detailed = true;
  } else {
    m_mon_basic = false;
    m_mon_detailed = false;
  }

  ERS_LOG(sout << "m_mon_basic = " << m_mon_basic);
  ERS_LOG(sout << "m_mon_detailed = " << m_mon_detailed);
  
  m_IS_ServerName = configuration->getString("IS_ServerName");
  ERS_LOG(sout << "m_IS_ServerName = " << m_IS_ServerName);

  m_IS_UpdatePeriod = configuration->getUInt("IS_UpdatePeriod");
  ERS_LOG(sout << "m_IS_UpdatePeriod = " << m_IS_UpdatePeriod);

  m_ServiceRequest_Limit = configuration->getUInt("ServiceRequest_Limit");
  ERS_LOG(sout << "m_ServiceRequest_Limit = " << m_ServiceRequest_Limit);

  m_ServiceRequest_ResetPeriod = configuration->getUInt("ServiceRequest_ResetPeriod");
  ERS_LOG(sout << "m_ServiceRequest_ResetPeriod = " << m_ServiceRequest_ResetPeriod);

  m_EmulateVME = configuration->getBool("EmulateVME");
  ERS_LOG(sout << "m_EmulateVME = " << m_EmulateVME);

  // get m_timeOutBusyFractionThreshold from database!
  m_timeOutBusyFractionThreshold = configuration->getDouble("TimeoutFraction");

  // Go through list of input channels and get: busyTimeout channel, id, UID
  // Then form mask
  m_mask_current = 0;
  m_mask_config = 0;
  Configuration* confDB = nullptr;
  try {
    confDB = configuration->getPointer<Configuration>("configurationDB");
  } catch (CORBA::SystemException& ex) {
    ostringstream text;
    text << sout << "Exception caught while creating configurationDB: " << ex._name();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  } catch (std::exception& ex) {
    ostringstream text;
    text << sout << "Exception caught while creating configurationDB: " << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  } catch ( ... ) {
    ostringstream text;
    text << sout << "Unknown exception caught and rethrown while creating configurationDB";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    throw;
  }

  // get partition object
  const RODBusydal::RODBusyModule* dbModule = confDB->get<RODBusydal::RODBusyModule>(m_UID);

  try {
    daq::rc::OnlineServices& os = daq::rc::OnlineServices::instance();
    const daq::core::Partition& partitionObject = os.getPartition();

    // first check whether the module itself is enabled
    if (!dbModule->daq::core::ResourceSet::disabled(partitionObject)) {
      ERS_LOG(sout << "RODBusyModule \"" << dbModule->UID() << "\" itself is enabled. Configuring busy mask.");
      
      // get the busy channels linked to "Contains"
      std::vector<const daq::core::ResourceBase*> items = dbModule->get_Contains();
      std::vector<const daq::core::ResourceBase*>::iterator it = items.begin();
      
      // IMPORTANT: disable all channels from the start
      for (int ic = 0; ic<16; ++ic) {
	m_enabled_config[ic] = false;
	m_enabled_current[ic] = false;
      }
      
      for(;it != items.end();it++){
	//check that it is a BusyChannel
	const RODBusydal::BusyChannel* bc = confDB->cast<RODBusydal::BusyChannel> (*it);
	if(bc){
	  
	  int channelnumber = bc->get_Id();
	  m_timeout_sec[channelnumber] = bc->get_BusyTimeout();
	  m_recovery_timeout_sec[channelnumber] = bc->get_RecoveryTimeout();
	  m_busychannel_uid[channelnumber] = bc->UID();

	  // check that busy channel is enabled
	  if (!bc->disabled(partitionObject)) {
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" (" << channelnumber << ") is enabled. checking corresponding busy source.");

	    // figure out whether the "BusySource" resource is enabled or disabled
	    const daq::core::ResourceBase* rb = bc->get_BusySource();
	    if (rb) {
	      // 
	      // still to do!!!
	      // need also check whether it is in the partition!!!!
	      // 
	      if (!rb->disabled(partitionObject)) {
		// resource is enabled
		if (channelnumber>=0 && channelnumber<16) {
		  ERS_LOG(sout << "Resource for " << bc->UID() << "@BusyChannel (Id=" << channelnumber << ") is enabled -> enabling channel");
		  m_enabled_config[channelnumber] = true;
		  m_enabled_current[channelnumber] = true;
		}
		else {
		  ostringstream text;
		  text << sout << "Invalid Id for " << bc->UID() << "@BusyChannel";
		  ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
		}
	      } 
	      else {
		ERS_LOG(sout << "Resource for BusyChannel \"" << bc->UID() << "\" (" << channelnumber << ") is disabled -> leaving channel disabled.");
	      }
	    }
	  } else {
	    // busy channel is disabled
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" (" << channelnumber << ") is disabled. Leaving busy channel disabled.");
	  }
	}
      }
    }
    else {
      // if disabled, do nothing about the busy mask
      ERS_LOG(sout << "RODBusyModule \"" << dbModule->UID() << "\" itself is disabled. Leaving all busy channels disabled.");
    }   
    
    // setup configuration mask
    m_mask_config = 0;
    unsigned int channel = 0;
    for (std::vector<bool>::const_iterator it = m_enabled_config.begin();
	 it != m_enabled_config.end();
	 ++it) {
      if (*it) {
	m_mask_config |= (0x1 << channel);
      }
      ++channel;
    }

    // update current mask accordingly
    this->updateBusyMask();

    ERS_LOG(sout << "Final busy mask (configuration) = 0x" << std::hex << m_mask_config << std::dec);
    ERS_LOG(sout << "Final busy mask (current) = 0x" << std::hex << m_mask_current << std::dec);

    // Get action plugins for recovery and timeout

    const RODBusydal::BusyTimeoutAction* db_timeout_action = dbModule->get_BusyTimeoutAction();
    const RODBusydal::BusyTimeoutAction* db_recovery_action = dbModule->get_BusyRecoveryAction();

    std::string class_name;

    // Load plug-in libraries
    class_name = db_timeout_action ? db_timeout_action->class_name () : "DefaultBusyTimeoutAction";
    m_timeoutFactory.load (class_name);
    m_timeout_plugin = m_timeoutFactory.create (class_name.c_str ());
    m_timeout_plugin->setup (confDB,
			     &partitionObject,
			     m_rcd_uid,
			     m_busychannel_uid,
			     m_enabled_config);
    ERS_LOG(sout << "Library " << class_name << " loaded");

    class_name = db_recovery_action ? db_recovery_action->class_name () : "DefaultBusyTimeoutAction";
    m_timeoutFactory.load (class_name);
    m_recovery_plugin = m_timeoutFactory.create (class_name.c_str ());
    m_recovery_plugin->setup (confDB,
			      &partitionObject,
			      m_rcd_uid,
			      m_busychannel_uid,
			      m_enabled_config);
    ERS_LOG(sout << "Library " << class_name << " loaded");
  } catch(daq::rc::OnlineServicesFailure& ex) {
    const std::string errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
    throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
void RODBusyModule::configure(const daq::rc::TransitionCmd&)
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::configure() ";
  ERS_LOG(sout << "Entered");

  m_status = 0;

  if (!m_EmulateVME) {
    m_rodbusy = new RCD::RODBusy();

    //Open RODBusy Module
    m_status |= m_rodbusy->Open(m_PhysAddress);

    if((m_status != VME_SUCCESS) || !m_rodbusy->CheckRODBusy()) {

      ostringstream text;
      text << sout << "Could not open RODBusy module at m_PhysAddress 0x" << std::hex << m_PhysAddress << std::dec;
      ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
    } 
  
    if (m_useConfiguration) {
      m_status |= m_rodbusy->Reset();
      m_status |= m_rodbusy->set_statusID(0);
      m_status |= m_rodbusy->set_IRQ_level(7);
      m_status |= m_rodbusy->enable_IRQ();
      m_status |= m_rodbusy->enable_SREQ();
      m_status |= m_rodbusy->assert_sw_busy();
      m_status |= m_rodbusy->setLimit(static_cast<u_short>(m_ServiceRequest_Limit));
      m_status |= m_rodbusy->setResetInterval(static_cast<u_short>(m_ServiceRequest_ResetPeriod));
      m_status |= m_rodbusy->setBusyState(0);
      m_status |= m_rodbusy->setBusyMask(m_mask_current);
      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_DISABLED);
    
      // 0xc34f = 49999, which corresponds to 5ms
      // the fifos are 512 words deep, which means the fifos should be
      // read out at least every 2.5 sec
      m_transferInterval = 0xC34F;
      m_status |= m_rodbusy->setSequencerTransferInterval(m_transferInterval);
 
      m_status |= m_rodbusy->release_sw_busy();
      m_status |= m_rodbusy->clear_SREQ();
      m_status |= m_rodbusy->resetCounter();
      m_status |= m_rodbusy->resetFifos();

      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_ENABLED);
    }


    if (m_mon_detailed) {
      m_PollAction = new PollAction(m_rodbusy, m_UID, m_EmulateVME);

      m_PollAction->setup_issue_reporting(m_busychannel_uid, m_rcd_uid);
      m_PollAction->disablePolling();
      m_PollAction->watch_set(m_timeOutBusyFractionThreshold, m_timeout_sec, m_recovery_timeout_sec, m_enabled_config);
      
      double watch_threshold(0);
      unsigned int watch_threshold_uint(0);
      m_PollAction->watch_getThreshold(watch_threshold, watch_threshold_uint);
      ERS_LOG(sout 
	      << "watch_threshold = " << watch_threshold
	      << ", watch_threshold_uint = 0x" << std::hex << watch_threshold_uint << std::dec);

      m_PollAction->watch_disable();
      m_PollAction->watch_reset();

      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_DISABLED);
      m_status |= m_rodbusy->release_sw_busy();
      m_status |= m_rodbusy->clear_SREQ();
      m_status |= m_rodbusy->resetCounter();
      m_status |= m_rodbusy->resetFifos();
      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_ENABLED);

      // Set the timeout and recovery plug-in

      m_PollAction->timeout_plugin_set (m_timeout_plugin);
      m_PollAction->recovery_plugin_set (m_recovery_plugin);

      m_PollAction->reset();
      m_PollAction->enablePolling();
    }
  }

  if(m_status != VME_SUCCESS) {
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void RODBusyModule::unconfigure(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::unconfigure() ";
  ERS_LOG(sout << "Entered");

  //Clean up memory.
  if (!m_EmulateVME) {
    if (m_mon_detailed) {
      m_PollAction->disablePolling();
      delete m_PollAction;
      m_PollAction=nullptr;
    }

    m_status = m_rodbusy->Close();

  }

  if(m_status != VME_SUCCESS) {
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void RODBusyModule::prepareForRun(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::prepareForRun() ";
  ERS_LOG(sout << "Entered");

  if (!m_EmulateVME) {

    // reset current busy mask to configuration busy mask
    m_mask_current = 0;
    for (unsigned int i = 0; i<16; ++i) {
      m_enabled_current[i] = m_enabled_config[i];
      if (m_enabled_current[i]) {
	m_mask_current |= (0x1 << i);
      }
    }
    m_status |= m_rodbusy->setBusyMask(m_mask_current);

    if (m_mon_detailed) {

      // reset
      m_PollAction->disablePolling();
      m_PollAction->watch_set(m_timeOutBusyFractionThreshold, m_timeout_sec, m_recovery_timeout_sec, m_enabled_config);
      
      double watch_threshold(0);
      unsigned int watch_threshold_uint(0);
      m_PollAction->watch_getThreshold(watch_threshold, watch_threshold_uint);
      ERS_LOG(sout 
	      << "watch_threshold = " << watch_threshold
	      << ", watch_threshold_uint = 0x" << std::hex << watch_threshold_uint << std::dec);
      m_PollAction->watch_disable();
      m_PollAction->watch_reset();
      
      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_DISABLED);
      m_status |= m_rodbusy->release_sw_busy();
      m_status |= m_rodbusy->clear_SREQ();
      m_status |= m_rodbusy->resetCounter();
      m_status |= m_rodbusy->resetFifos();
      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_ENABLED);
      
      m_PollAction->reset();
      m_PollAction->enablePolling();
    }
  }

  // subscribe to RootController run state for enabling/disabling watchdog mechanism
  this->subscribe();

  // poll run state once for the case that the RootController was already
  // in RUNNING state and therefore the rc_callback is never called
  try {
    m_isdict = new ISInfoDictionary(m_partition);
    RCStateInfo is_info;
    m_isdict->getValue(m_sub, is_info);
    this->handleRCStateInfo(is_info);
    
    delete m_isdict;
    m_isdict=nullptr;
  } catch ( daq::is::Exception & ex ) {
    ostringstream text;
    text << sout << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
 
  ERS_LOG(sout << "Done");
}

/**********************************************************/
void RODBusyModule::stopROIB(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::stopROIB() ";
  ERS_LOG(sout << "Entered");

  this->unsubscribe();

  ERS_LOG(sout << "Stopping watchdog");
  m_RC_in_running_state = false;
  this->stopWatchDog();

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void RODBusyModule::startWatchDog(void)
/**********************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::startWatchDog() ";
  ERS_LOG(sout << "Entered");

  if (!m_EmulateVME) {
    if (m_mon_detailed) {
      m_PollAction->watch_enable();
    }
  }

  if(m_status != VME_SUCCESS) {
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void RODBusyModule::stopWatchDog(void)
/**********************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::stopWatchDog() ";
  ERS_LOG(sout << "Entered");

  if (!m_EmulateVME) {

    // reset
    if (m_mon_detailed) {
      m_PollAction->watch_disable();
      m_PollAction->disablePolling();
      m_PollAction->watch_reset();

      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_DISABLED);
      m_status |= m_rodbusy->release_sw_busy();
      m_status |= m_rodbusy->clear_SREQ();
      m_status |= m_rodbusy->resetCounter();
      m_status |= m_rodbusy->resetFifos();
      m_status |= m_rodbusy->setMode(RCD::RODBusy::SEQ_COUNT_ENABLED);
      m_PollAction->enablePolling();
    }
  }

  if(m_status != VME_SUCCESS) {
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
DFCountedPointer < Config > RODBusyModule::getInfo()
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::getInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  DFCountedPointer<Config> configuration = Config::New();

  ERS_DEBUG(1, sout << "Done");

  return (configuration);
}

/******************************************************/
void RODBusyModule::clearInfo ()
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::clearInfo() ";
  ERS_DEBUG(1, sout << "Entered");
  ERS_DEBUG(1, sout << "Done");
}

/******************************************************/
void RODBusyModule::publishFullStats()
/******************************************************/
{  
  std::string sout = m_UID + "@RODBusyModule::publishFullStats() ";
  ERS_DEBUG(1, sout << "Entered");
  this->publish();
  ERS_DEBUG(1, sout << "Done");
}

/******************************************************/
void RODBusyModule::publish()
/******************************************************/
{  
  std::string sout = m_UID + "@RODBusyModule::publish() ";
  ERS_DEBUG(1, sout << "Entered");

  if (!m_EmulateVME) {
    if (m_mon_basic || m_mon_detailed) {
      RODBusyIS is_entry;
      is_entry.BusyOutputState = m_rodbusy->is_busy();
      is_entry.ChannelName = m_busychannel_uid;

      u_short mask(0);
      u_short state(0);
      m_status |= m_rodbusy->getBusyMask(mask);
      m_status |= m_rodbusy->getBusyState(state);
      is_entry.BusyEnabled.clear();
      is_entry.BusyChannelState.clear();
      u_short m=1;
  
      for (int i = 0; i < 16; ++i) {
	bool en = (m == (mask&m)) ? true : false;
	is_entry.BusyEnabled.push_back(en);
	en = (m == (state&m)) ? true : false;
	is_entry.BusyChannelState.push_back(en);
	m = m << 1;
      }

      is_entry.SoftwareBusyEnabled = m_rodbusy->sw_busy_is_asserted();
      is_entry.ServiceRequestEnabled = m_rodbusy->SREQ_is_enabled();
      is_entry.ServiceRequestActive = m_rodbusy->SREQ_is_active();

      u_short l(0);
      m_status |= m_rodbusy->getLimit(l);
      is_entry.ServiceRequestLimit = static_cast<u_int>(l);
      m_status |= m_rodbusy->getSequencerTransferInterval(l);
      is_entry.ServiceRequestResetPeriod = static_cast<u_int>(l);

      is_entry.VMEEmulation = m_EmulateVME;
  
      if (m_mon_detailed) {
	std::vector<unsigned int> timeout;
	std::vector<unsigned int> recovery_timeout;
	std::vector<unsigned int> counter;

	double watch_threshold(0);
	unsigned int watch_threshold_uint(0);
	m_PollAction->watch_get(watch_threshold, watch_threshold_uint,
				timeout, recovery_timeout, counter);
	// todo: add to IS entry
	is_entry.Timeout = timeout;
	is_entry.RecoveryTimeout = recovery_timeout;
	is_entry.TimeoutCounter = counter;

	u_int nwords(0);
      
	std::vector<double> rate(0);
	m_PollAction->getBusyRate(rate, nwords);
	is_entry.BusyFraction = rate;
	is_entry.TimeInterval = static_cast<double>(nwords)*0.005;
      }
      else {
	is_entry.Timeout.clear();
	is_entry.TimeoutCounter.clear();
	is_entry.BusyFraction.clear();
	for (int i = 0; i < 16; ++i) {
	  is_entry.Timeout.push_back(0);
	  is_entry.TimeoutCounter.push_back(0);
	  is_entry.BusyFraction.push_back(0.0);
	}
	is_entry.TimeInterval = 0.0;
      }

      ISInfoDictionary dict(m_partition);
      const std::string dict_entry = m_IS_ServerName + "." + m_UID + "/RODBusy";
      ERS_DEBUG(1, sout << dict_entry.c_str());
      try {
	dict.checkin(dict_entry, is_entry, true);
      } catch (CORBA::SystemException& ex) {
	ostringstream text;
	ERS_LOG(sout << "CORBA::SystemException caught while publishing to IS: \""<<ex._name()<<"\"");
      } catch (daq::is::Exception & ex) {
	ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
      } catch (std::exception & ex) {
	ostringstream text;
	ERS_LOG(sout << "std::exception caught while publishing to IS: \""<<ex.what()<<"\"");
      } catch (...) {
	ostringstream text;
	ERS_LOG(sout << "Unknown exception caught and rethrown while publishing to IS: \"" <<"\"");
	throw;
      }
    }
  }

  ERS_DEBUG(1, sout << "Done");
}

/******************************************************/
void RODBusyModule::updateBusyMask() {
  /******************************************************/
  m_mask_current = 0;
  unsigned int channel = 0;
  for (std::vector<bool>::const_iterator it = m_enabled_current.begin();
       it != m_enabled_current.end();
       ++it) {
    if (*it) {
      m_mask_current |= (0x1 << channel);
    }
    ++channel;
  }
}

/******************************************************/
void RODBusyModule::disable(const std::vector<std::string>& components) {
  /******************************************************/
  std::string sout = m_UID + "@RODBusyModule::disable() called with arguments ";
  std::string message = sout + "called with arguments ";
  for (size_t i = 0; i<components.size(); ++i) {
    message += "\"";
    message += components[i];
    message += "\" ";
  }
  ERS_LOG(message);

  // loop over the list and disable the corresponding busy channels
  for (std::vector<std::string>::const_iterator it1 = components.begin();
       it1 != components.end();
       ++it1) {
    // find corresponding busy channel
    int ichannel = 0;
    for (std::vector<string>::const_iterator it2 = m_busychannel_uid.begin();
	 it2 != m_busychannel_uid.end();
	 ++it2, ++ichannel) {
      if (*it1 == *it2) {
	// found matching names
	ERS_LOG(sout << "- found and disabling matching channel " << *it1);
	m_enabled_current[ichannel] = false;
      }
    }
  }

  this->updateBusyMask();

  if (!m_EmulateVME) {
    m_status |= m_rodbusy->setBusyMask(m_mask_current);
  }

  if(m_status != VME_SUCCESS) {
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "done.");
}

/******************************************************/
void RODBusyModule::enable(const std::vector<std::string>& components) {
  /******************************************************/
  std::string sout = m_UID + "@RODBusyModule::enable() called with arguments ";
  std::string message = sout + "called with arguments ";
  for (size_t i = 0; i<components.size(); ++i) {
    message += "\"";
    message += components[i];
    message += "\" ";
  }
  ERS_LOG(message);

  // loop over the list and enable the corresponding busy channels
  for (std::vector<std::string>::const_iterator it1 = components.begin();
       it1 != components.end();
       ++it1) {
    // find corresponding busy channel
    int ichannel = 0;
    for (std::vector<string>::const_iterator it2 = m_busychannel_uid.begin();
	 it2 != m_busychannel_uid.end();
	 ++it2, ++ichannel) {
      if (*it1 == *it2) {
	// found matching names
	ERS_LOG(sout << "- found and enabling matching channel " << *it1);
	m_enabled_current[ichannel] = true;
      }
    }
  }

  this->updateBusyMask();
 
  if (!m_EmulateVME) {
    m_status |= m_rodbusy->setBusyMask(m_mask_current);
  }

  if(m_status != VME_SUCCESS) { 
    ostringstream text;
    text << sout << "Could not write to VME";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }
  else    // Added by EP, May 2011 - needed to re-enable the watchdog on channels
  {
    unsigned int channel = 0;
    for (std::vector<bool>::const_iterator it = m_enabled_current.begin();
	 it != m_enabled_current.end(); ++it)
    {
      if (*it)
	m_PollAction->watch_enable (channel);

      ++channel;
    }
  }

  ERS_LOG(sout << "done.");
}

/******************************************************/
void RODBusyModule::userCommand(std::string &commandName, std::string &argument)
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::userCommand() ";
  ERS_LOG(sout << "entered.");

  std::cout << std::dec << "Received userCommand \"" << commandName.c_str() << "\" with argument \"" << argument << "\"" << std::endl;

  if (commandName == "DISABLE") {
    std::vector<std::string> vs;
    vs.push_back(argument);
    this->disable(vs);
  } else if (commandName == "ENABLE") {
    std::vector<std::string> vs;
    vs.push_back(argument);
    this->enable(vs);
  }

  ERS_LOG(sout << "done.");
}


/******************************************************/
void RODBusyModule::subscribe()
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::subscribe() ";

  if (m_isrec) {
    delete m_isrec;
    m_isrec = nullptr;
  }

  try {
    m_isrec = new ISInfoReceiver(m_partition);
    m_isrec->subscribe(m_sub, &RODBusyModule::rc_callback, this);  
  } catch ( daq::is::Exception & ex ) {
    ostringstream text;
    text << sout << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
}


/******************************************************/
void RODBusyModule::unsubscribe()
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::unsubscribe() ";
  if (m_isrec) {
    try {
      m_isrec->unsubscribe(m_sub);
    } catch ( daq::is::Exception & ex ) {
      ostringstream text;
      text << sout << ex.what();
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
    delete m_isrec;
    m_isrec = nullptr;
  }
}

/******************************************************/
void RODBusyModule::rc_callback(ISCallbackInfo* isc)
/******************************************************/
{
  RCStateInfo is_info;
  if (isc->reason() != is::Deleted) {
    isc->value(is_info);
    this->handleRCStateInfo(is_info);
  } else {
    return;
  }
}


/******************************************************/
void RODBusyModule::handleRCStateInfo(RCStateInfo& is_info) 
/******************************************************/
{
  std::string sout = m_UID + "@RODBusyModule::handleRCStateInfo() ";

  if (!m_RC_in_running_state) {
    if(is_info.state == "RUNNING") {
      ERS_LOG(sout << "Starting watchdog");
      m_RC_in_running_state = true;
      this->startWatchDog();
    }
  } else {
    if(is_info.state != "RUNNING") {
      ERS_LOG(sout << "Stopping watchdog");
      m_RC_in_running_state = false;
      this->stopWatchDog();
    }
  }

}


/******************************************************/
std::vector<std::string> RODBusyModule::parseCommaDeliminatedStrings(std::string s)
/******************************************************/
{
  std::vector<std::string> helper;
  std::string temp;

  while (s.find(",", 0) != std::string::npos)
    {
      size_t  pos = s.find(",", 0); 
      temp = s.substr(0, pos);      
      s.erase(0, pos + 1);
      helper.push_back(temp);         
    }
  helper.push_back(s);
  return helper;
}


extern "C" 
{
  extern ReadoutModule *createRODBusyModule ();
}

ReadoutModule *createRODBusyModule ()
{
  return (new RODBusyModule ());
}



