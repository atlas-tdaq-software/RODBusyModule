#include <iostream>
#include <string>
#include <vector>

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"

#include "ROSUtilities/PluginFactory.h"

#include "RODBusyModule/BusyTimeoutAction.h"
#include "RODBusyModule/DefaultBusyTimeoutAction.h"

#include "RODBusydal/BusyChannel.h"
#include "RODBusydal/BusyTimeoutAction.h"
#include "RODBusydal/RODBusyModule.h"

#include "config/Configuration.h"
#include "dal/app-config.h"
#include "dal/ResourceBase.h"
#include "dal/Partition.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include "DFdal/ReadoutApplication.h"

#define ERROR     1
#define NO_ERROR  0

Configuration *oks_db = 0;

bool g_debug = false;
char *g_partition_name = 0;
std::string g_module_name;

int test_rodbusy_action_loop ()
{
  // Get partition object
  try {
    daq::rc::OnlineServices& os = daq::rc::OnlineServices::instance();
    const daq::core::Partition& partitionObject = os.getPartition();

    //  const daq::core::Partition *partitionObject = daq::core::get_partition (*oks_db, g_partition_name);

    // Get module object(s)

    std::vector <std::string> rcd;
    std::vector <const RODBusydal::RODBusyModule *> modules;
    std::set <std::string> app_types;

    // Get all RCD applications

    app_types.insert ("RCD");

    std::vector<const daq::core::BaseApplication*> apps = partitionObject.get_all_applications (&app_types, nullptr, nullptr);
    std::vector<const daq::core::BaseApplication*>::iterator iapp;

    bool found = false;

    for (iapp = apps.begin (); iapp != apps.end (); ++iapp)
      {
	std::string iname = (*iapp)->UID();
	size_t at_pos = iname.find ("@");

	if (at_pos != std::string::npos)
	  iname.erase (at_pos, std::string::npos);

	const daq::df::ReadoutApplication *rapp = oks_db->get <daq::df::ReadoutApplication> (iname);

	if (g_debug)
	  std::cout << rapp->UID () << std::endl;

	// Get all the RODBusyModule objects

	std::vector <const daq::core::ResourceBase *> res = rapp->get_Contains ();
	std::vector <const daq::core::ResourceBase *>::iterator ires;

	for (ires = res.begin (); ires != res.end (); ++ires)
	  {
	    const RODBusydal::RODBusyModule *bm = oks_db->cast <RODBusydal::RODBusyModule> (*ires);

	    if (bm)
	      {
		if (g_debug)
		  std::cout << "        " << bm->UID () << std::endl;

		if (! (*ires)->disabled(partitionObject))
		  {
		    if (g_module_name.size () == 0)
		      {
			rcd.push_back (rapp->UID ());
			modules.push_back (bm);
		      }
		    else if (bm->UID () == g_module_name)
		      {
			rcd.push_back (rapp->UID ());
			modules.push_back (bm);
			found = true;
			break;
		      }
		  }
	      }
	  }

	if (found)
	  break;
      }
  
    std::vector <std::string>::iterator ircd;
    std::vector <const RODBusydal::RODBusyModule *>::iterator imod;
    
    for (imod = modules.begin (), ircd = rcd.begin (); imod != modules.end (); ++imod, ++ircd)
      {
	std::vector <std::string> busychannel_uid;
	std::vector <bool> enabled;
	
	int k;
	
	for (k=0; k < 16; ++k)
	  {
	    std::string uid = "invalid";
	    busychannel_uid.push_back (uid);
	    enabled.push_back (false);
	  }

	// Get the busy channels linked to "Contains"
	
	std::vector <const daq::core::ResourceBase *> items = (*imod)->get_Contains ();
	std::vector <const daq::core::ResourceBase *>::iterator it;

	for (it = items.begin () ; it != items.end () ; it++)
	  {
	    // Check that it is a BusyChannel
	    
	    const RODBusydal::BusyChannel *bc = oks_db->cast <RODBusydal::BusyChannel> (*it);
	    
	    if (bc)
	      {
		int channelnumber = bc->get_Id ();
		busychannel_uid[channelnumber] = bc->UID ();
		enabled[channelnumber] = true;
	      }
	  }
	
	// Get action plugins for recovery and timeout
	
	ROS::BusyTimeoutAction *timeout_plugin;
	const RODBusydal::BusyTimeoutAction *db_timeout_action = (*imod)->get_BusyTimeoutAction();

	// Load plug-in libraries

	std::string class_name = db_timeout_action ? db_timeout_action->class_name () : "DefaultBusyTimeoutAction";
	
	if (g_debug)
	  std::cout << "Loading " << class_name << std::endl;

	PluginFactory <ROS::BusyTimeoutAction> timeoutFactory;
	timeoutFactory.load (class_name);
	timeout_plugin = timeoutFactory.create (class_name.c_str ());

	if (g_debug)
	  std::cout << "Calling " << class_name << " setup" << std::endl;
	
	timeout_plugin->setup (oks_db,
			       &partitionObject,
			       (*ircd),
			       busychannel_uid,
			       enabled,
			       g_debug);

	if (g_debug)
	  std::cout << "Library " << class_name << " loaded and set up" << std::endl;
	
	std::cout << "Module " << (*imod)->UID () << std::endl;
	
	for (it = items.begin () ; it != items.end () ; it++)
	  {
	    // Check that it is a BusyChannel
	    
	    const RODBusydal::BusyChannel *bc = oks_db->cast <RODBusydal::BusyChannel> (*it);
	    
	    if (bc)
	      {
		int channelnumber = bc->get_Id ();
		std::string channelname = busychannel_uid[channelnumber];
		std::string controller = timeout_plugin->get_controller (channelname);
		std::vector <std::pair <std::string, std::string> > sources;
		
		bool disable_watchdog = timeout_plugin->get_busy_source (channelname, sources);
		
		std::vector <std::pair <std::string, std::string> >::iterator isource;
		
		for (isource = sources.begin (); isource != sources.end (); ++isource)
		  {
		    std::pair <std::string, std::string> s = *isource;
		    
		    std::cout << "    " << busychannel_uid[channelnumber] << " "
			      << controller << " " << s.second << std::endl;
		  }
		
		std::cout << "    watchdog would be "
			  << (disable_watchdog ? "disabled" : "enabled") << std::endl;
	      }
	  }
      }

  } catch(daq::rc::OnlineServicesFailure& ex) {
    const std::string errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
    throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
  }

  return (NO_ERROR);
}

void test_rodbusy_action_help ()
{
  std::cout << "Usage: test_rodbusy_action [-m module_name] "
	    << "[-p partition] [-d] [-h]"
            << std::endl;

  exit (NO_ERROR);
}

int test_rodbusy_action_opt (int argc, char **argv)
{
  int i = 1;

  for ( ; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'd':
          g_debug = true;
          break;
        case 'm':
          g_module_name = argv[++i];
          break;
        case 'p':
          g_partition_name = argv[++i];
          break;
        case 'h':
          test_rodbusy_action_help ();
          break;
        default:
          test_rodbusy_action_help ();
          break;
      }

  if (g_partition_name == (char *) nullptr)
    if ((g_partition_name = getenv ("TDAQ_PARTITION")) == (char *) nullptr)
    {
      test_rodbusy_action_help ();
      return ERROR;
    }

  return NO_ERROR;
}

int test_rodbusy_action_init (int argc, char **argv)
{
  // Initialize ipc for plug-in usage

  try
  {
    IPCCore::init (argc, argv);
  }

  catch (daq::ipc::Exception &ex)
  {
    ers::fatal (ex);
    return ERROR;
  }

  oks_db = new Configuration ("");

  if (!oks_db->loaded ())
  {
    delete oks_db;

    const ers::LocalContext cont ("RODBusy", __FILE__,
                                  __LINE__, __FUNCTION__);

    daq::config::Generic ex (cont, "OKS configuration database not loaded");
    ers::fatal (ex);

    return ERROR;
  }

  return NO_ERROR;
}

int test_rodbusy_action_end ()
{
  delete oks_db;

  return NO_ERROR;
}

int main (int argc, char **argv)
{
  if (test_rodbusy_action_opt (argc, argv) == ERROR)
    return (ERROR);

  if (test_rodbusy_action_init (argc, argv) == ERROR)
    return (ERROR);

  if (test_rodbusy_action_loop () == ERROR)
  {
    test_rodbusy_action_end ();
    return (ERROR);
  }

  if (test_rodbusy_action_end () == ERROR)
    return (ERROR);

  return NO_ERROR;
}
